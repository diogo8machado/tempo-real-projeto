
const axios = require('axios')
let name = process.argv[2]||`Paciente ${Math.floor(Math.random()*1000)}`
let idade = process.argv[3]||Math.floor(Math.random()*80)

setInterval(() => {
  let diastolica;
  let sistolica
  let frequencia
  let diastolicaAlerta;
  let sistolicaAlerta
  let frequenciaAlerta

  if(idade<3){
    diastolica=aleatorio(25,30,15)
    sistolica=aleatorio(50,52,15)
    diastolicaAlerta=verificar(25,30,diastolica)
    sistolicaAlerta=verificar(50,52,sistolica)
  }else if((idade>=3)&&(idade<10)){
    diastolica=aleatorio(46,78,20)
    sistolica=aleatorio(78,114,20)
    diastolicaAlerta=verificar(46,78,diastolica)
    sistolicaAlerta=verificar(78,114,sistolica)
  }else if((idade>=10)&&(idade<16)){
    diastolica=aleatorio(56,86,20)
    sistolica=aleatorio(90,132,20)
    diastolicaAlerta=verificar(56,86,diastolica)
    sistolicaAlerta=verificar(90,132,sistolica)
  }else if((idade>=16)&&(idade<20)){
    diastolica=aleatorio(60,92,30)
    sistolica=aleatorio(104,108,30)
    diastolicaAlerta=verificar(60,92,diastolica)
    sistolicaAlerta=verificar(104,108,sistolica)
  }else if((idade>=20)&&(idade<52)){
    diastolica=aleatorio(60,90,30)
    sistolica=aleatorio(95,140,30)
    diastolicaAlerta=verificar(60,90,diastolica)
    sistolicaAlerta=verificar(95,140,sistolica)
  }else{
    diastolica=aleatorio(70,90,30)
    sistolica=aleatorio(140,160,30)
    diastolicaAlerta=verificar(70,90,diastolica)
    sistolicaAlerta=verificar(140,160,sistolica)
  }

  if(idade<1){
    frequencia=aleatorio(70,170,40)
    frequenciaAlerta=verificar(70,170,frequencia)
  }else if((idade>=1)&&(idade<2)){
    frequencia=aleatorio(80,160,40)
    frequenciaAlerta=verificar(80,160,frequencia)
  }else if((idade>=2)&&(idade<4)){
    frequencia=aleatorio(80,130,40)
    frequenciaAlerta=verificar(80,130,frequencia)
  }else if((idade>=4)&&(idade<6)){
    frequencia=aleatorio(80,120,40)
    frequenciaAlerta=verificar(80,120,frequencia)
  }else if((idade>=6)&&(idade<8)){
    frequencia=aleatorio(75,115,40)
    frequenciaAlerta=verificar(75,115,frequencia)
  }else if((idade>=8)&&(idade<20)){
    frequencia=aleatorio(70,110,40)
    frequenciaAlerta=verificar(70,110,frequencia)
  }else{
    frequencia=aleatorio(60,100,40)
    frequenciaAlerta=verificar(60,100,frequencia)
  }
  date=new Date().toString();
  paciente={name,diastolica,
          diastolicaAlerta,sistolica,
          sistolicaAlerta,frequencia,
          frequenciaAlerta,idade,date}
  axios.post('http://localhost:5000/paciente',paciente)
  axios.post('http://localhost:5000/alerta',paciente)
}, 1000);

aleatorio=(max,min,passo)=>{
  let valor
  let random=Math.random()
  if(random<=0.02){
    valor= (Math.floor(-(Math.random() * passo) + min));
  }else if((random>0.02)&&(random<0.98)){
    valor= (Math.floor((Math.random() * (max-min)) + min));
  }else{
    valor= (Math.floor((Math.random() * passo) + max));
  }
  return valor

}

const verificar=(baixa,alta,valor)=>{
  if(valor<baixa){
    estado='baixa'
  }else if((valor>=baixa)&&(valor<=alta)){
    estado='normal'
  }else{
    estado='alta'
  }
  return estado;
}