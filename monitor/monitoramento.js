const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const http = require('http')
const server = http.createServer(app);
const WebSocket = require('ws')
const restful = require('node-restful')
const mongoose = restful.mongoose
mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost:27017/TempoReal', {useNewUrlParser: true})

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.use(cors())

// ODM
const Alerta = restful.model('Alerta', {
  name: { type: String},
  diastolica:{type:Number},
  diastolicaAlerta:{type:String},
  sistolica:{type:Number},
  sistolicaAlerta:{type:String},
  frequencia:{type:Number},
  frequenciaAlerta:{type:String},
  idade:{type:Number},
  date:{type:String}
})
// Rest API
Alerta.methods(['get', 'post', 'put', 'delete'])
Alerta.updateOptions({new: true, runValidators: true})

// Routes
Alerta.register(app, '/alerta') 

const wss = new WebSocket.Server({server})
let pacientes=[]
wss.on('connection', ws => {
  ws.on('message', message => {
    console.log(`Received message => ${message}`)
  })
  ws.send(JSON.stringify({type:'lista',data:pacientes}))
})

app.post('/paciente', function (req, res) {
  console.log(req.body)
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify({type:'alerta',data:req.body}));;
    }
  });
  let pertence=false
  pacientes.forEach((paciente)=>{
    if(req.body.name==paciente.name){
      pertence=true;
      paciente.ativo=true;
    }
  })
 
  if(pertence===false){
    pacientes.push({name:req.body.name,ativo:true})
    wss.clients.forEach(function each(client) {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify({type:'lista',data:pacientes}));;
      }
    });
  }
  res.send('POST request to the homepage');
  
});
//verifica se o paciente ainda esta
setInterval(() => {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify({type:'lista',data:pacientes}));;
    }
  });
  pacientes.forEach((paciente,index)=>{
    if(paciente.ativo===false){
      pacientes.splice(index, 1)
    }else{
      paciente.ativo=false
    }
  })
}, 5000);

const path = require('path');

app.use(express.static(__dirname+'/tela'));
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname+'/tela','hospital.html'));
});




console.log('serve 5000')
server.listen(5000)