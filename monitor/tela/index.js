
const url = 'wss://regimapp.site/'
const connection = new WebSocket(url)

connection.onopen = () => {
  connection.send('hey')
}

connection.onerror = error => {
  console.log(`WebSocket error: ${error}`)
}

connection.onmessage = e => {
  data=JSON.parse(e.data)
  if(data.type=='lista'){
    criarLista(data.data)
  }
  if(data.type=='alerta'){
    alerta(data.data)
  }
}

alerta=(alerta)=>{
  pai=document.getElementById(alerta.name)
  if(pai!==null){
    const alertaP = document.createElement('p')
    verificar(alerta.name,alerta.diastolica,alerta.diastolicaAlerta,'Diastólica')
    verificar(alerta.name,alerta.sistolica,alerta.sistolicaAlerta,'Sistólica')
    verificar(alerta.name,alerta.frequencia,alerta.frequenciaAlerta,'Frequência')
    alertaP.className = 'log'
    alertaP.innerHTML=`Freq: ${alerta.frequencia} | Pressão: ${alerta.sistolica} - ${alerta.diastolica}`
    
    pai.insertBefore(alertaP, pai.firstChild)
  }

}

verificar=(paciente,valor,alerta,tipo)=>{
  const alertaP = document.createElement('p')
  if((alerta=='alta')||((alerta=='baixa'))){
    alertaP.className = 'log'
    alertaP.className = 'alerta'
    alertaP.innerHTML=`Alerta! ${tipo} ${alerta} :${valor}`
    let audio=new Audio('./bep.wav')
    audio.type = 'audio/wav';

    var playPromise = audio.play();

    if (playPromise !== undefined) {
      playPromise.then(function () {
      }).catch(function (error) {
      });
    }
    alertaBox=document.getElementById('popAlertaBox')
    const elem = document.createElement('div')
    elem.className = 'popAlerta'

    const popAlertaText = document.createElement('div')
    popAlertaText.className = 'pacienteName'
    popAlertaText.innerHTML=`Alerta ${paciente}! ${tipo} ${alerta} :${valor}`
    elem.appendChild(popAlertaText)
    alertaBox.appendChild(elem)
    pai.insertBefore(alertaP, pai.firstChild)

    setTimeout(() => {
      alertaP.classList.add("disable");
      elem.parentNode.removeChild(elem);
    }, 5000);
  }
}



criarLista=lista=>{
  pai=document.getElementById('lista')
  lista.forEach((paciente,index) => {
    if(!document.getElementById(paciente.name)){
      const elem = document.createElement('div')
      elem.className = 'pacienteBox'

      const titulo = document.createElement('div')
      titulo.className = 'pacienteName'
      titulo.innerHTML=`${paciente.name}`
      titulo.addEventListener("click", ()=>{ViewPaciente(paciente.name)});
      elem.appendChild(titulo)
  
      const pacienteLog = document.createElement('div')
      pacienteLog.className = 'pacienteLog'
      pacienteLog.id = paciente.name
      elem.appendChild(pacienteLog)
  
      pai.appendChild(elem)
    }
    if(paciente.ativo===false){
      let elem = document.getElementById(paciente.name).parentElement;
      elem.parentNode.removeChild(elem);
    }
  });
}
let pacienteModal;
let modal;
ViewPaciente=(name)=>{
  modal(name)
  pacienteModal=name
}

hiddenModal=()=>{
  let elem = document.getElementById('modal');
  elem.parentNode.removeChild(elem);
}
modal=(name)=>{
  pai=document.getElementById('appModal')
  const elem = document.createElement('div')
  elem.className = 'pacienteModal'
  elem.id='modal'
  const container = document.createElement('div')
  container.className = 'containerModal'
  container.id='container'
  const box = document.createElement('div')
  box.className = 'pacienteModalBox'
  axios.get('https://regimapp.site/alerta',{params:{name}})
  .then(function (res) {
    res.data.forEach((item)=>{
      const p = document.createElement('p')
      date=new Date(item.date)
      date=date.toLocaleString('pt-BR', { timeZone: 'UTC' })
      p.innerHTML=`${date} - Freq: ${item.frequencia} | Pressão: ${item.sistolica} - ${item.diastolica}`
      box.insertBefore(p, box.firstChild)
    })
    cancelar=document.createElement('button')
    cancelar.innerHTML="Voltar"
    cancelar.addEventListener("click", hiddenModal);
    container.appendChild(box)
    container.appendChild(cancelar)
    elem.appendChild(container)
    document.getElementById('appModal').appendChild(elem)
  })
  .catch(function (error) {
  })
  
  // const titulo = document.createElement('div')
  // titulo.className = 'pacienteName'
  // titulo.innerHTML=`${paciente.name}`
  // elem.appendChild(titulo)

  // const pacienteLog = document.createElement('div')
  // pacienteLog.className = 'pacienteLog'
  // pacienteLog.id = paciente.name
  // elem.appendChild(pacienteLog)
}

