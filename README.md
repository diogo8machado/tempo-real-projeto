# Projeto de Tempo real (Finalizado)

-------------

## O Sistema

Tem como objetivo monitorar simultaneamente dados vitais vários pacientes e alertar caso ocorra alguma anormalidade nos dados.

A arquitetura do sistema divide-se em 3 partes:

- Monitores
- Servidor
- Aplicação

### Aplicativo

Uma pagina web para disponibilizar os dados dos paciente e alertar caso tenha uma problema com o paciente.

Conexão com o server atravez de websocket.

### Servidor

Servidor terá o trabalho de centralizar todas as informações recebidas dos monitores sobre os pacientes e disponibiliza-las para conexões websocket abertas dos aplicativos. Feito em Nodejs(Javascript).

### Monitores

Neste o projeto os monitores estão sendo simulados, tem como objetivo enviar dados do paciente para o servirdor.
